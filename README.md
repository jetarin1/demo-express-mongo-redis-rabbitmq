# E-commerce API

This is a simple e-commerce API built with Node.js, Express.js, MongoDB, Redis, and RabbitMQ. The API allows you to add products, retrieve product details with caching, and place orders which are processed asynchronously.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running the Application](#running-the-application)
- [API Endpoints](#api-endpoints)
- [Testing](#testing)
- [License](#license)

## Prerequisites

- Docker and Docker Compose
- Node.js (version 16)

## Installation

1. **Clone the repository**

    ```sh
    git clone https://github.com/your-username/ecommerce-api.git
    cd ecommerce-api
    ```

2. **Install Node.js dependencies**

    ```sh
    npm install
    ```

3. **Create a `.nvmrc` file**

    Ensure that the Node.js version 16 is specified in the `.nvmrc` file:

    ```sh
    echo "16" > .nvmrc
    ```

## Running the Application

1. **Start Docker services**

    Start MongoDB, Redis, and RabbitMQ using Docker Compose:

    ```sh
    docker-compose up -d
    ```

2. **Start the Node.js application**

    ```sh
    node index.js
    ```

## API Endpoints

### Add a Product

- **URL:** `/products`
- **Method:** `POST`
- **Request Body:**

    ```json
    {
      "name": "Sample Product",
      "description": "This is a sample product.",
      "price": 100,
      "stock": 50
    }
    ```

- **Response:**

    ```json
    {
      "id": "60c72b2f9b1d4c001c8e4b0d"
    }
    ```

### Get Product Details

- **URL:** `/products/:id`
- **Method:** `GET`
- **Response:**

    ```json
    {
      "product": {
        "_id": "60c72b2f9b1d4c001c8e4b0d",
        "name": "Sample Product",
        "description": "This is a sample product.",
        "price": 100,
        "stock": 50
      },
      "cache": "hit"
    }
    ```

### Place an Order

- **URL:** `/orders`
- **Method:** `POST`
- **Request Body:**

    ```json
    {
      "productId": "60c72b2f9b1d4c001c8e4b0d",
      "quantity": 1
    }
    ```

- **Response:**

    ```json
    "Order placed"
    ```

## Testing

You can use `curl` to test the endpoints:

1. **Add a Product**

    ```sh
    curl -X POST http://localhost:3000/products -H "Content-Type: application/json" -d '{
      "name": "Sample Product",
      "description": "This is a sample product.",
      "price": 100,
      "stock": 50
    }'
    ```

2. **Get Product Details**

    ```sh
    curl http://localhost:3000/products/PRODUCT_ID
    ```

3. **Place an Order**

    ```sh
    curl -X POST http://localhost:3000/orders -H "Content-Type: application/json" -d '{
      "productId": "PRODUCT_ID",
      "quantity": 1
    }'
    ```

## License

This project is licensed under the MIT License.
