// index.js
const express = require("express");
const mongoose = require("mongoose");
const amqp = require("amqplib");
const Redis = require("ioredis");

const app = express();
const port = 3000;

// MongoDB connection
mongoose.connect("mongodb://localhost:27017/ecommerce", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

// Redis connection
const redisClient = new Redis({
  host: "localhost",
  port: 6379,
});

redisClient.on("error", (err) => {
  console.log("Redis error: ", err);
});
redisClient.on("connect", () => {
  console.log("Connected to Redis");
});

// RabbitMQ connection
let channel;
async function connectRabbitMQ() {
  try {
    const connection = await amqp.connect("amqp://localhost");
    channel = await connection.createChannel();
    await channel.assertQueue("order_queue", { durable: true });
    console.log("Connected to RabbitMQ");
    processOrders(); // Start processing orders once connected
  } catch (error) {
    console.error("RabbitMQ connection error:", error);
    setTimeout(connectRabbitMQ, 5000); // Retry connection after 5 seconds
  }
}
connectRabbitMQ();

// Define models for MongoDB
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  name: String,
  description: String,
  price: Number,
  stock: Number,
});
const Product = mongoose.model("Product", ProductSchema);

const OrderSchema = new Schema({
  product: { type: Schema.Types.ObjectId, ref: "Product" },
  quantity: Number,
  status: { type: String, default: "pending" },
});
const Order = mongoose.model("Order", OrderSchema);

// Express routes
app.use(express.json());

app.get("/", (req, res) => {
  res.send("E-commerce API");
});

// Add a product
app.post("/products", async (req, res) => {
  const { name, description, price, stock } = req.body;
  const newProduct = new Product({ name, description, price, stock });
  try {
    const savedProduct = await newProduct.save();
    res.status(201).json({ id: savedProduct._id }); // Respond with the ID of the saved product
  } catch (error) {
    console.error("Error saving product:", error);
    res.status(500).send("Error saving product");
  }
});

// Get a product
app.get("/products/:id", async (req, res) => {
  const { id } = req.params;

  // Check Redis cache first
  redisClient.get(id, async (err, result) => {
    if (err) {
      console.error("Redis get error: ", err);
      return res.status(500).send("Redis error");
    }
    if (result) {
      res.json({ product: JSON.parse(result), cache: "hit" });
    } else {
      // If not in cache, fetch from MongoDB
      try {
        const product = await Product.findById(id);
        if (product) {
          // Cache the result in Redis
          redisClient.set(id, JSON.stringify(product), "EX", 3600); // Cache for 1 hour
          res.json({ product, cache: "miss" });
        } else {
          res.status(404).send("Product not found");
        }
      } catch (error) {
        console.error("Error retrieving product from MongoDB:", error);
        res.status(500).send("Error retrieving product");
      }
    }
  });
});

// Place an order
app.post("/orders", async (req, res) => {
  const { productId, quantity } = req.body;

  if (!channel) {
    return res.status(500).send("Order service unavailable");
  }

  // Send order to RabbitMQ queue
  const order = { productId, quantity };
  channel.sendToQueue("order_queue", Buffer.from(JSON.stringify(order)), {
    persistent: true,
  });

  res.status(201).send("Order placed");
});

// Process orders from the queue
async function processOrders() {
  if (!channel) {
    console.error("Cannot process orders: RabbitMQ channel is not initialized");
    return;
  }
  channel.consume("order_queue", async (msg) => {
    if (msg !== null) {
      const orderData = JSON.parse(msg.content.toString());
      const product = await Product.findById(orderData.productId);
      if (product && product.stock >= orderData.quantity) {
        product.stock -= orderData.quantity;
        await product.save();
        const newOrder = new Order({
          product: product._id,
          quantity: orderData.quantity,
          status: "completed",
        });
        await newOrder.save();
        console.log(
          `Order processed: ${orderData.productId} x ${orderData.quantity}`
        );
      } else {
        console.log("Order failed: insufficient stock");
      }
      channel.ack(msg);
    }
  });
}

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
